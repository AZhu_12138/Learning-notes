#### [剑指 Offer II 068. 查找插入位置](https://leetcode-cn.com/problems/N6YdxV/)

难度简单

给定一个排序的整数数组 `nums` 和一个整数目标值` target` ，请在数组中找到 `target `，并返回其下标。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。

请必须使用时间复杂度为 `O(log n)` 的算法。

 

**示例 1:**

```
输入: nums = [1,3,5,6], target = 5
输出: 2
```

**示例 2:**

```
输入: nums = [1,3,5,6], target = 2
输出: 1
```

**示例 3:**

```
输入: nums = [1,3,5,6], target = 7
输出: 4
```

**示例 4:**

```
输入: nums = [1,3,5,6], target = 0
输出: 0
```

**示例 5:**

```
输入: nums = [1], target = 0
输出: 0
```

 

**提示:**

- `1 <= nums.length <= 104`
- `-104 <= nums[i] <= 104`
- `nums` 为**无重复元素**的**升序**排列数组
- `-104 <= target <= 104`



**思路：**

排序数组的查找：二分查找



**代码：**

```java
public int searchInsert(int[] nums, int target) {
    int left = 0, right = nums.length - 1;
    while (left <= right) {
        int mid = (left + right) / 2;
        if (nums[mid] == target) {
            return mid;
        } else if (nums[mid] < target) {
            left = mid + 1;
        } else {
            right = mid - 1;
        }
    }
    return left;
}
```



#### [剑指 Offer II 069. 山峰数组的顶部](https://leetcode-cn.com/problems/B1IidL/)

难度简单

符合下列属性的数组 `arr` 称为 **山峰数组**（**山脉数组）** ：

- `arr.length >= 3`

- 存在

   

  ```
  i
  ```

  （

  ```
  0 < i < arr.length - 1
  ```

  ）使得：

  - `arr[0] < arr[1] < ... arr[i-1] < arr[i]`
  - `arr[i] > arr[i+1] > ... > arr[arr.length - 1]`

给定由整数组成的山峰数组 `arr` ，返回任何满足 `arr[0] < arr[1] < ... arr[i - 1] < arr[i] > arr[i + 1] > ... > arr[arr.length - 1]` 的下标 `i` ，即山峰顶部。

 

**示例 1：**

```
输入：arr = [0,1,0]
输出：1
```

**示例 2：**

```
输入：arr = [1,3,5,4,2]
输出：2
```

**示例 3：**

```
输入：arr = [0,10,5,2]
输出：1
```

**示例 4：**

```
输入：arr = [3,4,5,1]
输出：2
```

**示例 5：**

```
输入：arr = [24,69,100,99,79,78,67,36,26,19]
输出：2
```

 

**提示：**

- `3 <= arr.length <= 104`
- `0 <= arr[i] <= 106`
- 题目数据保证 `arr` 是一个山脉数组

 

**进阶：**很容易想到时间复杂度 `O(n)` 的解决方案，你可以设计一个 `O(log(n))` 的解决方案吗？



**思路：**

二分查找，mid 在数组中的位置无非三种情况：

- 刚好在山顶
- 在山顶的左侧
- 在山顶的右侧



**代码：**

```java
public int peakIndexInMountainArray(int[] arr) {
    int left = 0, right = arr.length - 1;
    while (left <= right) {
        int mid = (left + right) / 2;
        if (mid == 0) {
            return 1;
        }
        if (arr[mid] > arr[mid - 1] && arr[mid] > arr[mid + 1]) {
            return mid;
        } else if (arr[mid] <= arr[mid - 1]) {
            right = mid - 1;
        } else {
            left = mid + 1;
        }
    }
    return -1;
}
```



#### [剑指 Offer II 070. 排序数组中只出现一次的数字](https://leetcode-cn.com/problems/skFtm2/)

难度中等

给定一个只包含整数的有序数组 `nums` ，每个元素都会出现两次，唯有一个数只会出现一次，请找出这个唯一的数字。

 

**示例 1:**

```
输入: nums = [1,1,2,3,3,4,4,8,8]
输出: 2
```

**示例 2:**

```
输入: nums =  [3,3,7,7,10,11,11]
输出: 10
```

 

**提示:**

- `1 <= nums.length <= 105`
- `0 <= nums[i] <= 105`

 

**进阶:** 采用的方案可以在 `O(log n)` 时间复杂度和 `O(1)` 空间复杂度中运行吗？



**思路：**

二分查找，因此需要根据 mid 在数组中的位置情况来判断出 目标数 位于 mid 的左还是右。

因为 left 和 right 之间的差值可能是 2 的奇数倍或偶数倍，都会导致 mid 的位置相差 1 ，所以需要根据 left 和 right 之差来固定 mid 的位置，然后根据 mid 元素和左右两旁的元素的相等关系来判断 目标数 在左还是右，然后移动相对 mid 两个位置。再继续循环判断。

另外，还需要注意 `nums.length == 1`、 `mid == 0`和`mid == nums.length`这两个特殊情况。



**代码：**

```java
public int singleNonDuplicate(int[] nums) {
    // 特殊情况
    if (nums.length == 1) {
        return nums[0];
    }
    int n = nums.length - 1;
    int left = 0, right = nums.length - 1;
    while (left <= right) {
        int mid = (left + right) / 2;
        mid = mid % 2 == 0 ? mid : mid - 1; // 使mid相对固定，才能判断 目标数 位于mid的左还是右。
        // 特殊情况
        if (mid == 0) {
            if (nums[mid] != nums[mid + 1]) {
                return nums[mid];
            } else {
                return nums[mid + 2];
            }
        }
        if (mid == n) {
            if (nums[mid] != nums[mid - 1]) {
                return nums[mid];
            } else {
                return nums[mid - 2];
            }
        }
        // 根据mid元素和左右两旁的元素的相等关系来判断 目标数 在左还是右，移动相对mid两个位置
        if (nums[mid] != nums[mid - 1] && nums[mid] != nums[mid + 1]) {
            return nums[mid];
        } else if (nums[mid] == nums[mid + 1]) {
            left = mid + 2;
        } else {
            right = mid - 2;
        }
    }
    return -1;
}
```



#### [剑指 Offer II 071. 按权重生成随机数](https://leetcode-cn.com/problems/cuyjEf/)

难度中等

给定一个正整数数组 `w` ，其中 `w[i]` 代表下标 `i` 的权重（下标从 `0` 开始），请写一个函数 `pickIndex` ，它可以随机地获取下标 `i`，选取下标 `i` 的概率与 `w[i]` 成正比。



例如，对于 `w = [1, 3]`，挑选下标 `0` 的概率为 `1 / (1 + 3) = 0.25` （即，25%），而选取下标 `1` 的概率为 `3 / (1 + 3) = 0.75`（即，75%）。

也就是说，选取下标 `i` 的概率为 `w[i] / sum(w)` 。

 

**示例 1：**

```
输入：
inputs = ["Solution","pickIndex"]
inputs = [[[1]],[]]
输出：
[null,0]
解释：
Solution solution = new Solution([1]);
solution.pickIndex(); // 返回 0，因为数组中只有一个元素，所以唯一的选择是返回下标 0。
```

**示例 2：**

```
输入：
inputs = ["Solution","pickIndex","pickIndex","pickIndex","pickIndex","pickIndex"]
inputs = [[[1,3]],[],[],[],[],[]]
输出：
[null,1,1,1,1,0]
解释：
Solution solution = new Solution([1, 3]);
solution.pickIndex(); // 返回 1，返回下标 1，返回该下标概率为 3/4 。
solution.pickIndex(); // 返回 1
solution.pickIndex(); // 返回 1
solution.pickIndex(); // 返回 1
solution.pickIndex(); // 返回 0，返回下标 0，返回该下标概率为 1/4 。

由于这是一个随机问题，允许多个答案，因此下列输出都可以被认为是正确的:
[null,1,1,1,1,0]
[null,1,1,1,1,1]
[null,1,1,1,0,0]
[null,1,1,1,0,1]
[null,1,0,1,0,0]
......
诸若此类。
```

 

**提示：**

- `1 <= w.length <= 10000`
- `1 <= w[i] <= 10^5`
- `pickIndex` 将被调用不超过 `10000` 次

 

**思路：**

使用一个变量 total 记录 w[] 元素总和；数组 sums[] 记录 w[] 元素的前缀和，对应 [ 0 , total ) 之间的若干个区间。

然后生成一个位于 [ 0 , total )  的随机数，随机数位于区间哪里就返回该处的索引。



**代码：**

```java
class Solution {

    private int[] sums; // 数组w[]的前缀和数组
    private int total; // 数组w[]的元素总和，也是sums[]的最后一个元素。

    public Solution(int[] w) {
        sums = new int[w.length];
        sums[0] = w[0];
        for (int i = 1; i < sums.length; i++) {
            sums[i] = sums[i - 1] + w[i];
        }
        total = sums[sums.length - 1];
    }

    public int pickIndex() {
        int rand = new Random().nextInt(total); // 位于 [0,total) 的随机值
        int left = 0, right = sums.length - 1;
        while (left <= right) {
            int mid = (left + right) / 2;
            if (rand < sums[mid] && (mid == 0 || sums[mid - 1] <= rand)) {
                return mid;
            } else if (rand >= sums[mid]) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return left;
    }
}
```



#### [剑指 Offer II 072. 求平方根](https://leetcode-cn.com/problems/jJ0w9p/)

难度简单

给定一个非负整数 `x` ，计算并返回 `x` 的平方根，即实现 `int sqrt(int x)` 函数。

正数的平方根有两个，只输出其中的正数平方根。

如果平方根不是整数，输出只保留整数的部分，小数部分将被舍去。

 

**示例 1:**

```
输入: x = 4
输出: 2
```

**示例 2:**

```
输入: x = 8
输出: 2
解释: 8 的平方根是 2.82842...，由于小数部分将被舍去，所以返回 2
```

 

**提示:**

- `0 <= x <= 231 - 1`



**思路：**

可以知道：x 的正数平方根必定在 `[0 , x]`之中。

因此，在 `[0 , x]`之上采用二分查找：

- `mid^2 <= x` 且 `(mid + 1)^2 > x`，则 mid 就是结果
- `mid^2 <= x` 且 `(mid + 1)^2 <= x`，则结果位于 mid 的左侧
- `mid^2 > x`，则结果位于 mid 的右侧



**代码：**

```java
public int mySqrt(int x) {
    int left = 0, right = x;
    while (left <= right) {
        int mid = left + (right - left) / 2; // 防止数据溢出
        if (mid > x / mid) { // 等价于 mid*mid > x
            right = mid - 1;
        } else if ((mid + 1) <= x / (mid + 1)) { // 等价于 (mid + 1)*(mid + 1) <= x
            left = mid + 1;
        } else {
            return mid;
        }
    }
    return -1;
}
```



#### [剑指 Offer II 073. 狒狒吃香蕉](https://leetcode-cn.com/problems/nZZqjQ/)

难度中等

狒狒喜欢吃香蕉。这里有 `N` 堆香蕉，第 `i` 堆中有 `piles[i]` 根香蕉。警卫已经离开了，将在 `H` 小时后回来。

狒狒可以决定她吃香蕉的速度 `K` （单位：根/小时）。每个小时，她将会选择一堆香蕉，从中吃掉 `K` 根。如果这堆香蕉少于 `K` 根，她将吃掉这堆的所有香蕉，然后这一小时内不会再吃更多的香蕉，下一个小时才会开始吃另一堆的香蕉。 

狒狒喜欢慢慢吃，但仍然想在警卫回来前吃掉所有的香蕉。

返回她可以在 `H` 小时内吃掉所有香蕉的最小速度 `K`（`K` 为整数）。

 

**示例 1：**

```
输入: piles = [3,6,7,11], H = 8
输出: 4
```

**示例 2：**

```
输入: piles = [30,11,23,4,20], H = 5
输出: 30
```

**示例 3：**

```
输入: piles = [30,11,23,4,20], H = 6
输出: 23
```

 

**提示：**

- `1 <= piles.length <= 10^4`
- `piles.length <= H <= 10^9`
- `1 <= piles[i] <= 10^9`



**思路：**

可以知道：k 的最小值为`sum / h`，否则就肯定吃不完这些香蕉；k 的最大值为数组中最大值`max`，这样就必定能吃完香蕉。

所以在`sum / h`和`max`之间用二分法枚举 k 值：

- 当 mid 不能吃完，则在右侧
- 当 mid 能吃完，则可能在左侧，或就是该 mid 值。因此记录此时的 mid ，再往左二分枚举。



**代码：**

```java
public class Solution {

    public int minEatingSpeed(int[] piles, int h) {
        int sum = 0, max = 0; // 求数组和、数组中元素最大值。
        for (int i = 0; i < piles.length; i++) {
            sum += piles[i];
            max = Math.max(max, piles[i]);
        }
        int left = sum / h, right = max;
        int res = max;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (canEatUp(piles, h, mid)) {
                res = Math.min(res, mid);
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return res;
    }

    private boolean canEatUp(int[] piles, int h, int k) {
        if (k <= 0) {
            return false;
        }
        for (int i = 0; i < piles.length; i++) {
            if (h <= 0) { // 时间用完，但是没吃完
                return false;
            }
            h -= piles[i] / k;
            if (piles[i] % k != 0) {
                --h;
            }
        }
        return h >= 0;
    }

}
```



#### [剑指 Offer II 074. 合并区间](https://leetcode-cn.com/problems/SsGoHC/)

难度中等

以数组 `intervals` 表示若干个区间的集合，其中单个区间为 `intervals[i] = [starti, endi]` 。请你合并所有重叠的区间，并返回一个不重叠的区间数组，该数组需恰好覆盖输入中的所有区间。

 

**示例 1：**

```
输入：intervals = [[1,3],[2,6],[8,10],[15,18]]
输出：[[1,6],[8,10],[15,18]]
解释：区间 [1,3] 和 [2,6] 重叠, 将它们合并为 [1,6].
```

**示例 2：**

```
输入：intervals = [[1,4],[4,5]]
输出：[[1,5]]
解释：区间 [1,4] 和 [4,5] 可被视为重叠区间。
```

 

**提示：**

- `1 <= intervals.length <= 104`
- `intervals[i].length == 2`
- `0 <= starti <= endi <= 104`



**思路：**

区间1 和 区间2 有重合的依据是：区间1的头 <= 区间2的头，且，区间1的尾 >= 区间2的头。

则合并区间得到新区间，新区间的头为区间1的头，新区间的尾为 max(区间1的尾，区间2的尾)。



**代码：**

```java
public int[][] merge(int[][] intervals) {
    // 根据区间的起始位置排序对区间排序
    Arrays.sort(intervals, (i1, i2) -> i1[0] - i2[0]);
    LinkedList<int[]> res = new LinkedList<>();
    int i = 0, left, right;
    while (i < intervals.length) {
        left = intervals[i][0];
        right = intervals[i][1];
        // 有重合区间
        while (++i < intervals.length && right >= intervals[i][0]) {
            right = Math.max(right, intervals[i][1]);
        }
        // 得到合并后的区间
        res.add(new int[]{left, right});
    }
    return res.toArray(new int[res.size()][]);
}
```



#### [剑指 Offer II 076. 数组中的第 k 大的数字](https://leetcode-cn.com/problems/xx4gT2/)

难度中等

给定整数数组 `nums` 和整数 `k`，请返回数组中第 `**k**` 个最大的元素。

请注意，你需要找的是数组排序后的第 `k` 个最大的元素，而不是第 `k` 个不同的元素。

 

**示例 1:**

```
输入: [3,2,1,5,6,4] 和 k = 2
输出: 5
```

**示例 2:**

```
输入: [3,2,3,1,2,4,5,5,6] 和 k = 4
输出: 4
```

 

**提示：**

- `1 <= k <= nums.length <= 104`
- `-104 <= nums[i] <= 104`



**思路：**

使用快排的切分方法，由于一次切分后，就能使得切分元素放到了正确的位置，并返回了该切分元素的位置。

可以知道，题目要求的第 k 大的数的位置就是 `nums.length - k`。

所以我们对数组进行切分，得到切分位置为 p ：

- 若 p 等于 `nums.length - k`，则返回结果
- 若 p 小于`nums.length - k`，则在 p 的左边部分进行切分
- 若 p 大于`nums.length - k`，则在 p 的右边部分进行切分

若每次选取的切分元素都接近数组的中间位置（中间值），那么时间复杂度为：`n + n/2 + n/4 + n/8 + ... + 1 = 2n` 所以是 `O(n)`。



**代码：**

```java
public class Solution {

    public int findKthLargest(int[] nums, int k) {
        int target = nums.length - k;
        int left = 0, right = nums.length - 1;
        while (left <= right) {
            int p = partition(nums, left, right);
            if (p == target) {
                return nums[p];
            } else if (p < target) {
                left = p + 1;
            } else {
                right = p - 1;
            }
        }
        return nums[left];
    }

    private int partition(int[] nums, int left, int right) {
        int rand = new Random().nextInt(right - left + 1) + left;
        swap(nums, rand, right);
        int p1 = left - 1;
        for (int p2 = left; p2 < right; p2++) {
            if (nums[p2] < nums[right]) {
                ++p1;
                swap(nums, p1, p2);
            }
        }
        ++p1;
        swap(nums, p1, right);
        return p1;
    }

    private void swap(int[] nums, int i1, int i2) {
        if (i1 != i2) {
            int temp = nums[i1];
            nums[i1] = nums[i2];
            nums[i1] = temp;
        }
    }

}
```



#### [剑指 Offer II 077. 链表排序](https://leetcode-cn.com/problems/7WHec2/)

难度中等

给定链表的头结点 `head` ，请将其按 **升序** 排列并返回 **排序后的链表** 。



 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2020/09/14/sort_list_1.jpg)

```
输入：head = [4,2,1,3]
输出：[1,2,3,4]
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2020/09/14/sort_list_2.jpg)

```
输入：head = [-1,5,3,4,0]
输出：[-1,0,3,4,5]
```

**示例 3：**

```
输入：head = []
输出：[]
```

 

**提示：**

- 链表中节点的数目在范围 `[0, 5 * 104]` 内
- `-105 <= Node.val <= 105`

 

**进阶：**你可以在 `O(n log n)` 时间复杂度和常数级空间复杂度下，对链表进行排序吗？



**思路：**

用归并排序：

1. 将链表对半切开两半
2. 对两半进行递归的排序
3. 合并排序好的两半

合并只需要改变指针，不需要额外的空间，但是递归调用需要栈空间，所以空间复杂度为：O(logn) 。

归并排序时间复杂度为：O(nlogn) 。



**代码：**

```java
public class Solution {

    public ListNode sortList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        // 1.切开两半
        ListNode h1 = head;
        ListNode h2 = split(head);
        // 2.对两半排序
        h1 = sortList(h1);
        h2 = sortList(h2);
        // 3.合并排序好的两半
        return merge(h1, h2);
    }

    /*
    对链表head平均分隔成两部分，返回后半部分的头节点
     */
    private ListNode split(ListNode head) {
        ListNode slow = head, fast = head.next;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        ListNode h2 = slow.next;
        slow.next = null; // 切开
        return h2;
    }

    /*
    合并两个已经有序的链表h1和h2
     */
    private ListNode merge(ListNode h1, ListNode h2) {
        ListNode res = new ListNode();
        ListNode p = res;
        while (h1 != null || h2 != null) {
            if (h2 == null || (h1 != null && h1.val <= h2.val)) {
                p.next = h1;
                h1 = h1.next;
            } else {
                p.next = h2;
                h2 = h2.next;
            }
            p = p.next;
        }
        return res.next;
    }
}
```



#### [剑指 Offer II 078. 合并排序链表](https://leetcode-cn.com/problems/vvXgSW/)

难度困难

给定一个链表数组，每个链表都已经按升序排列。

请将所有链表合并到一个升序链表中，返回合并后的链表。

 

**示例 1：**

```
输入：lists = [[1,4,5],[1,3,4],[2,6]]
输出：[1,1,2,3,4,4,5,6]
解释：链表数组如下：
[
  1->4->5,
  1->3->4,
  2->6
]
将它们合并到一个有序链表中得到。
1->1->2->3->4->4->5->6
```

**示例 2：**

```
输入：lists = []
输出：[]
```

**示例 3：**

```
输入：lists = [[]]
输出：[]
```

 

**提示：**

- `k == lists.length`
- `0 <= k <= 10^4`
- `0 <= lists[i].length <= 500`
- `-10^4 <= lists[i][j] <= 10^4`
- `lists[i]` 按 **升序** 排列
- `lists[i].length` 的总和不超过 `10^4`



**思路：**

两重的归并

存储多个链表头节点的数组 lists 本质上也是一个数组，因此对 lists 进行子数组的归并。

在对子数组归并的过程中，就是对相邻链表的归并。



**代码：**

```java
public class Solution {

    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }
        return mergeLists(lists, 0, lists.length);
    }

    private ListNode mergeLists(ListNode[] lists, int left, int right) {
        if (left >= right - 1) {
            return lists[left];
        }
        int mid = (left + right) / 2;
        ListNode h1 = mergeLists(lists, left, mid);
        ListNode h2 = mergeLists(lists, mid, right);
        return merge(h1, h2);
    }

    /*
    合并两个已经有序的链表h1和h2
     */
    private ListNode merge(ListNode h1, ListNode h2) {
        ListNode res = new ListNode();
        ListNode p = res;
        while (h1 != null || h2 != null) {
            if (h2 == null || (h1 != null && h1.val <= h2.val)) {
                p.next = h1;
                h1 = h1.next;
            } else {
                p.next = h2;
                h2 = h2.next;
            }
            p = p.next;
        }
        return res.next;
    }
}
```






