# Oracle增量插入(merge)

## 背景

在许多场景中，我们需要在一张表中插入数据：对于表中不存在的数据，我们应该插入；但是对于表中已经存在的数据，我们则不插入而是更新。

增量插入的意思就是：对于新来的数据则插入，对于已经存在的数据不插入而是更新。

如果我们使用普通的方法，就需要在插入数据前先`select`，查看表中是否有此数据，再决定是执行插入还是更新操作。但是这样做就十分的繁琐且效率低下。

幸好，Oracle 可以使用`merge`语句对表进行数据的“合并、融合”操作，可以轻松地实现“无则插入，有则更新”的操作。

## 使用方法

基本语法：

```sql
merge into [target-table] A 
using [source-table sql] B 
on( [expression]and[...] )
when matched then
	[update sql]
when not matched then
	[insert sql]
```

作用：判断A表和B表是否满足`on`中的条件，如果满足则用B表去更新A表，如果不满足，则将B表数据插入A表。

注：

- A是目标表`target-table`的别名，B是对原始表`source-table`进行`sql`查询之后的结果集的别名。
- `source-table sql`可以是任意的`select`语句，包括内连接外连接。
- `on( )`里面写判断表达式，一般是`A.id=B.id`类型。
- `matched`和`not matched`子句可以只选其一。
- `update`和`insert`是可以带`where`条件判断的。

## 测试用例

```sql
MERGE INTO YZYD_QY_SJZJ_RECORD A 
USING (
    select s.XKID, s.XKZBH, s.P_LAST_UPDATE_TIME, s.UPDATE_TIME 
    from SH_1001001 s
    where s.XKZZT='未过期' and s.YXQZ is null
) B 
ON (A.XKID=B.XKID)
WHEN MATCHED THEN
	update set A.XKZBH=B.XKZBH, A.P_LAST_UPDATE_TIME=B.P_LAST_UPDATE_TIME, A.UPDATE_TIME=B.UPDATE_TIME, A.CWYY='主表中存在，但是许可证状态(XKZZT)为未过期，有效期至(YXQZ)为null'
WHEN NOT MATCHED THEN
	insert(XKID, XKZBH, CWYY, P_LAST_UPDATE_TIME, UPDATE_TIME)
	values(B.XKID, B.XKZBH, '主表中存在，但是许可证状态(XKZZT)为未过期，有效期至(YXQZ)为null', B.P_LAST_UPDATE_TIME, B.UPDATE_TIME);
```

