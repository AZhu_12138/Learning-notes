# Oracle存储过程Navicat

## 如何创建一个存储过程？

1、函数->右键->新建函数

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210810161909.png)

2、选择“过程”

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210810162212.png)

3、设置输入输出的形式参数：

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210810162538.png)

4、之后它就会自动生成一个存储过程的模板，我们只需要在模板上写自己的代码。

```sql
CREATE OR REPLACE
PROCEDURE "test_procedure" (innum IN INT DEFAULT '1', gen OUT VARCHAR)
AS
BEGIN
	-- routine body goes here, e.g.
	-- DBMS_OUTPUT.PUT_LINE('Navicat for Oracle');
END;
```

5、写好保存就可以了。

## 讲讲界面的按钮

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210810163308.png)

保存之后就可以点击运行，运行存储过程。

## 使用存储过程

### 点击按钮运行

1、输入实际参数

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210810163538.png)

2、运行结果：

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210810163810.png)

### 在sql中调用

```sql
declare
	定义变量
begin
	调用存储过程
end;
```

例子：

```sql
declare
	num number;
	gen VARCHAR2(10);
begin
	num:=12;
	test_procedure(num,gen);
end;
```

