## distinct

`distinct`只能将`select`后面的全部字段作为一个合字段，只有合字段重复的行才会被去重。

## row_number()和over()

在oracle数据库中，为咱们提供了一个函数 row_number() 用于给数据库表中的记录进行标号，在使用的时候，其后还跟着一个函数 over()，而函数 over() 的作用是将表中的记录进行分组和排序。两者使用的语法为：

```sql
row_number() over(partition by col1 order by col2)
```

意为：将表中的记录按字段 col1 进行分组，按字段 col2 进行排序，其中

partition by：表示分组

order by：表示排序

```sql
select * from (
	select h.XKID, h.XKZBH, h.P_LAST_UPDATE_TIME, h.UPDATE_TIME, row_number() over(partition by h.XKID order by h.		P_LAST_UPDATE_TIME desc) rn
	from SH_H_YPSCXKZ h, SH_1001001 s
	where h.XKID=s.XKID(+) and s.XKID is null and h.XKZZT='未过期'
)where rn=1
```

在原表的基础上，多了一列标有数字排序的列：rn

rn 的数值代表着该列在自己组内的排名。

