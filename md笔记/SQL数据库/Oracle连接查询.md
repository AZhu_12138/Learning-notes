## 使用

```sql
select h.XKID, h.XKZBH, h.P_LAST_UPDATE_TIME, h.UPDATE_TIME
from SH_H_YPSCXKZ h, SH_1001001 s
where h.XKID=s.XKID(+) and s.XKID is null and h.XKZZT='未过期')
```

- `(+)`在`=`的右边就是左连接
- `(+)`在`=`的左边就是右连接

