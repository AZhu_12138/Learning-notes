## 前言

一般我们`git clone`项目都是使用`https`，这样的话每次`clone`起来就比较方便，但是每次`push`的时候都需要输入用户名和密码，就比较繁琐。

因此我们可以在开源网站的项目上配置我们的`ssh`公钥，这样每次`push`就不用输入用户名和密码了。

下面是具体步骤。

## 在本地生成密钥对

在任意位置`Git Bash Here`，

然后：

```bash
$ cd ~/.ssh
```

如果没有`.ssh`这个文件夹就自己创建一个。

然后使用自己的邮箱生成：

```bash
$ ssh-keygen -t rsa -C "xxx@xxx.com"
```

不用输入东西，直接按三次回车，就可以生成了。

再：

```bash
$ ll
```

就可以看到已经生成了两个文件：

- id_rsa：存放私钥，保密
- id_rsa.pub：存放公钥，可以公开

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210810094735.png)

## 在Gitee或者Github上配置公钥

首先：

```bash
$ cat id_rsa.pub
```

查看文件内容，将里面所有的内容`复制`起来。

### Gitee:

`粘贴`进去：

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210810095425.png)

然后回到本地：

```bash
$ ssh -T git@gitee.com
```

输入`yes`，回车，就可以啦！

### Github:

`粘贴`进去：

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210810095906.png)

然后回到本地：

```bash
$ ssh -T git@github.com
```

输入`yes`，回车，就可以啦！

## 使用ssh的url作为remote

### 还没有git clone

如果这时你本地还没有`git clone`，那么在你`git clone`的时候，就不要使用`https`的`url`来clone了，而是使用`ssh`的`url`。

如下图：

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210810100354.png)

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210810100500.png)

那么在你clone完成之后，以后的每次`push`都可以直接`push`了。

### 已经git clone

如果这时你本地已经`git clone`，那么就需要修改本地仓库关联的远程仓库的`url`。

首先在你项目下`Git Bash Here`，查看关联的`url`有哪些。

```bash
$ git remote -v
```

一般只有一个名为`origin`的`remote`。

就先删除这个`origin`，

```bash
$ git remote rm origin
```

然后再添加一个新的`origin`，

```bash
$ git remote add origin git@gitee.com:xxx/xxx.git
```

然后再查看一下：

```bash
$ git remote -v
```

这样就完成啦，如果你本地关联了多个`remote`，那就对你想修改的那个`remote`操作就好了。

