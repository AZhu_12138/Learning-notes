## 题目

https://leetcode-cn.com/problems/er-wei-shu-zu-zhong-de-cha-zhao-lcof/

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210827152532.png)

## 题解

**站在右上角看，这个矩阵其实就是一个二分查找树（Binary Search Tree）。**

那么本质上只是对于二分查找树查找：

1. 从右上角的根节点开始，比较结点和`target`的大小关系
   - 等于，返回`true`
   - 大于，进入左子结点，`--j`
   - 小于，进入右子结点，`++i`
2. 如此重复，注意循环的退出条件。

## 代码

```java
class Solution {
    public boolean findNumberIn2DArray(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0 || matrix[0] == null || matrix[0].length == 0) {
            return false;
        }
        int m = matrix.length, n = matrix[0].length;
        int i = 0, j = n - 1;
        while (i < m && j >= 0) {
            if (matrix[i][j] == target) {
                return true;
            } else if (matrix[i][j] > target) {
                --j;
            } else {
                ++i;
            }
        }
        return false;
    }
}
```

## 复杂度

时间复杂度：O(log mn)

空间复杂度：O(1)

