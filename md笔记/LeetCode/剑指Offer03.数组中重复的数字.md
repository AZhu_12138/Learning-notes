## 题目

https://leetcode-cn.com/problems/shu-zu-zhong-zhong-fu-de-shu-zi-lcof/

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210827144104.png)

## 题解

1. 核心：数字范围为`0 ~ n-1`，数组长度为`n`
2. 原地置换法：
3. 假设数字`x`必须位于`nums[x]`上，则为**合理**
4. 遍历数组，看看数组中的数字是否合理（在自己的位置上）
   - 若不合理，看看此时数字作为索引的位置上的数字是否合理
     - 若合理，发现重复数字，返回
     - 若不合理，与此时数字作为索引的位置交换数字，继续内循环
   - 若合理，则下一次外循环。

## 代码

```java
class Solution {
    public int findRepeatNumber(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int n = nums.length;
        for (int i = 0; i < n; i++) {
            while (nums[i] != i) {
                int target = nums[i];
                if (nums[target] == target) {
                    return target;
                } else {
                    nums[i] = nums[target];
                    nums[target] = target;
                } 
            }
        }
        return -1;
    }
}
```



## 复杂度

时间复杂度：O(n)

空间复杂度：O(1)

