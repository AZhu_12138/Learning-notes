# Vue项目创建

## 安装Cmder

因为后面的许多操作都需要用到`cmd`命令行，且win10自己的`cmd`或者`powershell`都不是很好用，所以这里**建议**安装一个很好用的命令行工具：**Cmder**。

当然，这个只是建议操作，不装这个也可以。

官网下载：[https://cmder.net/](https://cmder.net/)

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825104420.png)

无需安装，直接解压，打开`cmder.exe`即可使用。

但是每次使用都需要进入目录打开`cmder.exe`就十分的麻烦，所以我们就需要配置环境变量，使得在任意位置**右键**都可以打开`cmder`

####  配置右键快捷启动

将`cmder.exe`所在的路径添加到系统Path变量：

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825112454.png)

以管理员身份打开`powershell`：

```bash
# 设置任意地方鼠标右键启动Cmder
$ Cmder.exe /REGISTER ALL
```

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825112850.png)

完成：

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825112934.png)

## 安装Node.js和npm

因为`Node.js`自带`npm`，所以只需要安装`Node.js`。

官网下载：[http://nodejs.cn/download/](http://nodejs.cn/download/)

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825103505.png)

安装：指定安装路径，其余的默认就行。

测试是否安装成功：

```bash
$ node -v
$ npm -v
```

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825103653.png)

## 配置cnpm

大家都知道国内直接使用`npm`的官方镜像是非常慢的，这里推荐使用**淘宝NPM镜像**。

淘宝NPM镜像是一个完整`npmjs.org`镜像，你可以用此代替官方版本(只读)，同步频率目前为 10分钟 一次以保证尽量与官方服务同步。

你可以使用淘宝定制的`cnpm`(gzip 压缩支持) 命令行工具代替默认的`npm`：

```bash
$ npm install -g cnpm --registry=https://registry.npm.taobao.org
```

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825105036.png)

测试安装成功：

```bash
$ cnpm -v
```

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825105125.png)

## 安装vue-cli

Vue.js 提供的一个官方命令行工具，可用于快速搭建大型单页应用。

```bash
# 全局安装 vue-cli
$ cnpm install vue-cli -g
```

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825105300.png)

测试安装成功：

```bash
$ vue list
```

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825105347.png)

## 正式创建项目

在自己的项目目录下创建：

```bash
$ vue init webpack hellovue
```

这里需要进行一些配置，默认回车即可:

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825105642.png)

创建成功：

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825105858.png)

可以看到创建成功之后的提示信息，按照提示信息：进入项目，安装并运行

```bash
$ cd hellovue
$ cnpm run dev
```

启动成功：

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825110124.png)

成功启动之后访问：[http://localhost:8080/](http://localhost:8080/)

输出结果如下所示：

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825110508.png)

## 关闭项目

在启动成功的`cmd`界面：`Ctrl + C`

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825110713.png)

## 打包项目

打包 Vue 项目使用以下命令：

```bash
$ cnpm run build
```

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825111225.png)

执行完成后，会在 Vue 项目下生成一个`dist`目录，一般包含`index.html`文件及 `static `目录，`static `目录包含了静态文件 js、css 以及图片目录 images。

![](https://azhu12138.oss-cn-shenzhen.aliyuncs.com/img/20210825111147.png)

如果直接双击 `index.html` 打开浏览器，页面可能是空白了，想要修改下 `index.html` 文件中 js、css 路径即可。

例如我们打开 `dist/index.html` 文件看到路径是绝对路径：

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8><meta name=viewport content="width=device-width,initial-scale=1">
    <title>hellovue</title>
    <link href=/static/css/app.30790115300ab27614ce176899523b62.css rel=stylesheet>
  </head>
  <body>
    <div id=app></div>
    <script type=text/javascript src=/static/js/manifest.2ae2e69a05c33dfc65f8.js></script>
    <script type=text/javascript src=/static/js/vendor.b38abd1e8f82054d7d19.js></script>
    <script type=text/javascript src=/static/js/app.b22ce679862c47a75225.js></script>
  </body>
</html>
```

我们把 js、css 路径路径修改为相对路径：(把路径开头的 / 去掉)

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8><meta name=viewport content="width=device-width,initial-scale=1">
    <title>hellovue</title>
    <link href=static/css/app.30790115300ab27614ce176899523b62.css rel=stylesheet>
  </head>
  <body>
    <div id=app></div>
    <script type=text/javascript src=static/js/manifest.2ae2e69a05c33dfc65f8.js></script>
    <script type=text/javascript src=static/js/vendor.b38abd1e8f82054d7d19.js></script>
    <script type=text/javascript src=static/js/app.b22ce679862c47a75225.js></script>
  </body>
</html>
```

这样直接双击 `dist/index.html` 文件就可以在浏览器中看到效果了。

